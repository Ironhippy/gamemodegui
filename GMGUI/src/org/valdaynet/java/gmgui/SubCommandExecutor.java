package org.valdaynet.java.gmgui;

import java.util.HashMap;
import java.util.Map;

@Deprecated
public class SubCommandExecutor {
	
	private static Map<String, CMDOpen> map = new HashMap<String, CMDOpen>();
	
	public static CMDOpen getOpen(String arg0) {
		return map.get(arg0);
	}
}
