package org.valdaynet.java.gmgui;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class GameModeRegisterEvent implements Listener
{
	
	private Player p;
	@EventHandler
	public void onClick(InventoryClickEvent ev) {
		this.p = ((Player)ev.getWhoClicked());
		if (! (ev.getInventory().getTitle()==null) && ev.getInventory().getTitle().contains("GameMode GUI")) {
			if (ev.getCurrentItem().getItemMeta().getDisplayName().contains("Survival Mode")) {
				set(GameMode.SURVIVAL);
				ev.getWhoClicked().getOpenInventory().close();
			}
			else if (ev.getCurrentItem().getItemMeta().getDisplayName().contains("Creative Mode")) {
				set(GameMode.CREATIVE);
				UUnit.setOpen(p.getUniqueId().toString(), false);
				ev.getWhoClicked().getOpenInventory().close();
			}
		}
		ev.setCancelled(true);
	}
	private void set(GameMode arg0) {
		p.setGameMode(arg0);
		UUnit.sendTitle("Your game mode has been changed.", "Your game mode is now "+arg0.toString().toLowerCase(), p);
	}
}
