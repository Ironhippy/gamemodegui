package org.valdaynet.java.gmgui;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class GamemodeGUI extends JavaPlugin
{
	
	private static Economy econ = null;
	
	@Override
	public void onEnable()
	{
		getCommand("gmgui").setExecutor(new CoreCommand(this));
		getServer().getPluginManager().registerEvents(new GameModeRegisterEvent(), this);
		getServer().getPluginManager().registerEvents(new CloseEvent(), this);
		if (getEconomy()==null) {
			getServer().getPluginManager().disablePlugin(this);
		}
		else {
			econ = getEconomy();
		}
	}
	
	@Override
	public void onDisable() {
		getServer().getConsoleSender().sendMessage(ChatColor.GOLD+"GMGUI: "+ChatColor.AQUA+"Closing...");
	}
	public Economy getEconomy() {
		RegisteredServiceProvider<Economy> economy = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economy == null || economy.getProvider() == null) {
        	this.getLogger().severe("There is no economy provider to support Vault, make sure you installed an economy plugin");
        	return null;
        }		
	    return economy.getProvider();
	}
	
}