package org.valdaynet.java.gmgui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

public class CMDOpen 
{
	
	private Plugin plugin = Bukkit.getPluginManager().getPlugin("GMGUI");
	private Player p;
	
	public CMDOpen(Player player)
	{
		this.p = player;
	}
	
	public void execute() 
	{
		if (CoreCommand.getStart().getEconomy().getBalance(p)>=500)
		{
			CoreCommand.getStart().getEconomy().withdrawPlayer(p, 500);
		    final Inventory t = UUnit.getInterface(p);
		    UUnit.sendTitle("Loading Command GUI...", "", p);
		    UUnit.setOpen(p.getUniqueId().toString(), true);
		    Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			    @Override
			    public void run() {
				    p.openInventory(t);
			    }
		    }, 120);
		}
	}
	public void returnError(String arg0) 
	{
		UUnit.sendTitle("Could not load GUI.", "", p);
	}
}
