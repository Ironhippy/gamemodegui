package org.valdaynet.java.gmgui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class CoreCommand implements CommandExecutor {
	
	private CommandSender sender;
	private Command cmd;
	private String[] args;
	
	private static GamemodeGUI hui;
	
	public CoreCommand(GamemodeGUI gamemodeGUI) {
		hui = gamemodeGUI;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if (cmd.getName().equalsIgnoreCase("gmgui"))
		{
			if (sender.hasPermission("gmgui.use"))
			{
				this.sender = sender;
				this.cmd = cmd;
				this.args = args;
				if (args.length == 0) 
				{
					helpCmd();
				}
				else if (args.length >= 1)
				{
					if (args[0].equalsIgnoreCase("o") || args[0].equalsIgnoreCase("open"))
					{
						if (args.length == 1) 
						{
					        Player player = (Player) sender;
					        if (UUnit.ON==true) 
					        {
						        CMDOpen open = new CMDOpen(player);
						        open.execute();
					        }
					        else if (UUnit.ON==false)
					        {
					        	CMDOpen close = new CMDOpen(player);
					        	close.returnError("CMD GUI is not enabled");
					        }
						}
						else if (args.length >= 2) 
						{
							for (String arg : args) {
								@SuppressWarnings("deprecation")
								Player player = Bukkit.getPlayer(arg);
								CMDOpen open = new CMDOpen(player);
								open.execute();
							}
						}
					}
					else if (args[0].equalsIgnoreCase("e") || args[0].equalsIgnoreCase("enable")) 
					{
						
					}
				}
			}
		}
		return true;
	}
	private void helpCmd() {
		sender.sendMessage(ChatColor.GOLD+"_____.[ "+ChatColor.DARK_GREEN+"Help for command \""+cmd.getName()+"\""+ChatColor.GOLD+"1/1 ]._____\n"+
	                       ChatColor.AQUA+"/gmgui o, open "+ChatColor.DARK_AQUA+"[player=you] "+ChatColor.YELLOW+"open gui for said player");
	}
	public static GamemodeGUI getStart() {
		return hui;
	}
}
