package org.valdaynet.java.gmgui;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class UUnit
{
	
	public static boolean ON = true;
	public static boolean OPEN = false;
	
	private static Map<String, Boolean> map = new HashMap<String, Boolean>();
	
	@SuppressWarnings("deprecation")
	public static GameMode getGamemode(Object oid) 
	{
		GameMode mode = null;
		if (oid instanceof UUID) 
		{
			UUID id = (UUID) oid;
			OfflinePlayer p = Bukkit.getOfflinePlayer(id);
			if (! (p == null) && p.isOnline()) 
			{
				Player player = (Player) p;
				mode = player.getGameMode();
			}
		}
		else if (oid instanceof String) 
		{
			String str = (String) oid;
			OfflinePlayer p = Bukkit.getOfflinePlayer(str);
			if (! (p == null) && p.isOnline()) {
				Player player = (Player) p;
				mode = player.getGameMode();
			}
		}
		else if (oid instanceof Player) 
		{
			Player player = (Player) oid;
			mode = player.getGameMode();
		}
		else if (oid instanceof OfflinePlayer) 
		{
			OfflinePlayer p = (OfflinePlayer) oid;
			if (! (p == null) && p.isOnline())
			{
				Player player = (Player) p;
				mode = player.getGameMode();
			}
		}
		return mode;
	}
	public static Inventory getInterface(Player player) 
	{
		Inventory t = null;
		
		// Green Wool Data
		
		ItemStack greenStack = new Wool(DyeColor.GREEN).toItemStack(1);
		
		// Red Wool Data
		
		ItemStack redStack = new Wool(DyeColor.RED).toItemStack(1);
		
		// Green Wool Item Meta
		
		ItemMeta greenData = greenStack.getItemMeta();
		greenData.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Survival Mode");
		greenStack.setItemMeta(greenData);
		
		// Red Wool Item Meta
		
		ItemMeta redData = redStack.getItemMeta();
		redData.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Creative Mode");
		redStack.setItemMeta(redData);
		
		t = Bukkit.createInventory(player, 9, ChatColor.GOLD+""+ChatColor.BOLD+"GameMode GUI");
		t.setItem(0, greenStack);
		t.setItem(1, greenStack);
		t.setItem(2, greenStack);
		
		t.setItem(8, redStack);
		t.setItem(7, redStack);
		t.setItem(6, redStack);
		
		return t;
	}
	public static void sendTitle(String arg0, String arg1, Player p) {
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "title "+p.getName()+" title {\"text\":\""+arg0+"\",\"color\":\"yellow\"}");
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "title "+p.getName()+" subtitle {\"text\":\""+arg1+"\",\"color\":\"green\"}");
	}
	public static boolean setOpen(String name, boolean b) {
		map.put(name, b);
		return map.get(name);
	}
}
