package org.valdaynet.java.gmgui;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class CloseEvent implements Listener {
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent ev) 
	{
		if (! (ev.getInventory().getTitle() == null) && ev.getInventory().getName().contains("GameMode GUI") && UUnit.OPEN==true) 
		{
			ev.getPlayer().openInventory(ev.getInventory());
		}
	}

}
